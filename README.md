First of all thanks to xFileFIN for a good source of getting players from keeper

## How do I edit this?
Since this code is pretty easy and readable, you have to know basic JS knowledge to add more functions. Also you need to know docker, you can use any other hosting development apps and delete docker related files.
**Here is an Example** :point_right:
```js
// queue function for your server
function fetchMax(guid) {
    return fetch(`https://keeper.battlelog.com/snapshot/${guid}`)
        .then(res => res.json())
        .then(json => {
            if (json.snapshot.status == 'SUCCESS') {
                let queue = json.snapshot.waitingPlayers
                queue = `[${queue}]`;

                return queue
            } else {
                throw new Exception(`Invalid fetch status`);
            }
        });
}
```
## How I run this?
1. Edit code for your guid and bot's picture
2. type npm i to install all packages
3. run command **docker-compose up** for running it (for hosting) **npm run** for local tests

**24/7 hosting checks** [Example](https://i.imgur.com/iBFUqpU.png)
**Look of the Status** [Look](https://cdn.discordapp.com/attachments/712036548765286472/712340357853085706/unknown.png)

For any questions, bugs, better code examples please contact me through discord or gitlab