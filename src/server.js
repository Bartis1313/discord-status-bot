const discord = require('discord.js'); //in ecma import package from 'discord.js'; 
const client = new discord.Client(); //define it as you want
const keeper = require('./keeper'); //in ecma import keeper from './keeper'

client.login(process.env.DISCORD_TOKEN); // needs docker
// how to secure your token? token: /[\w]{24}\.[\w]{6}\.[\w-_]{27}/   used regex

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    setInterval(update, 2500);
});

async function update() {
    const guid = " " // add your guid into " " or add env to docker so process.env.GUID_SERVER for example...
    const server = await keeper.fetchPlayerCounts(guid);
    const map = await keeper.fetchMaps(guid);
    const mode = await keeper.fetchMode(guid);
    const max = await keeper.fetchMax(guid);
    if (server == 0) { //you can delete it if you want
        client.user.setActivity(`Server is empty`);
    }
    client.user.setActivity(`${server}/${max} players Current Map: ${map} with Mode: ${mode}`); // edit it as you want
    client.user.setAvatar(' '); // add your link for the bot's picture, 
}
client.on("guildCreate", guild => { // can delete too, but I do appreciate not deleting this :)

    let defaultChannel = "";
    guild.channels.cache.forEach((channel) => {
        if (channel.type == "text" && defaultChannel == "") {
            if (channel.permissionsFor(guild.me).has("SEND_MESSAGES")) {
                defaultChannel = channel;
            }
        }
    })
    defaultChannel.send(`Thanks for adding me! Author: **Bartis#1313**`)
});